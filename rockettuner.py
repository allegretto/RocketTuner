# RocketTuner - a tuner that rocks!
# Copyright (C) 2016-2021, Zsombor Hollay-Horvath (hollay.horvath@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gdk, Gst

from collections import namedtuple

default_freq = 440.0
default_temperament = 'Equal Temperament'
default_note = 9
default_octave = 4

highest_octave = 8
notes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']

temperaments = {
    'Equal Temperament': [
        (2.0 ** (1.0/12.0)) ** octave
        for octave in range(12)
    ],
    'Werckmeister I (III)': [
        1.0000000000000000, # C,  1/1
        1.0534979423868314, # C#, 256/243
        1.1174033085417048, # D,  64/81 * 2**(1/2)
        1.1851851851851851, # D#, 32/27
        1.2528272487271466, # E,  256/243 * 2**(1/4)
        1.3333333333333333, # F,  4/3
        1.4046639231824416, # F#, 1024/729
        1.4949269604510480, # G,  8/9 * 8**(1/4)
        1.5802469135802468, # G#, 128/81
        1.6704363316361952, # A,  1024/729 * 2**(1/4)
        1.7777777777777777, # A#, 16/9
        1.8792408730907195  # B,  128/81 * 2**(1/4)
    ],
    'Werckmeister II (IV)': [
        1.0000000000000000, # C,  1/1
        1.0487500117602806, # C#, 16384/19683 * 2**(1/3)
        1.1199298221287761, # D,  8/9 * 2**(1/3)
        1.1851851851851851, # D#, 32/27
        1.2542428064933920, # E,  64/81 * 4**(1/3)
        1.3333333333333333, # F,  4/3
        1.4046639231824416, # F#, 1024/729
        1.4932397628383682, # G,  32/27 * 2**(1/3)
        1.5731250176404208, # G#, 8192/6561 * 2**(1/3)
        1.6723237419911896, # A,  256/243 * 4**(1/3)
        1.7858261834642244, # A#, 9/(4 * 2**(1/3))
        1.8728852309099222  # B,  4096/2187
    ],
    'Werckmeister III (V)': [
        1.0000000000000000, # C,  1/1
        1.0570729911135297, # C#, 8/9*2**(1/4)
        1.1250000000000000, # D,  9/8
        1.1892071150027210, # D#, 2**(1/4)
        1.2570787221094177, # E,  8/9*2**(1/2)
        1.3378580043780612, # F,  9/8*2**(1/4)
        1.4142135623730951, # F#  2**(1/2)
        1.5000000000000000, # G,  3/2
        1.5802469135802468, # G#, 128/81
        1.6817928305074290, # A,  8**(1/4)
        1.7838106725040817, # A#, 3/(8**(1/4))
        1.8856180831641267  # B,  4/3*2**(1/2)
    ],
    'Werckmeister IV (VI)': [
        1.0000000000000000, # C,  1/1
        1.0537634408602150, # C#, 98/93
        1.1136363636363635, # D,  49/44
        1.1878787878787880, # D#, 196/165
        1.2564102564102564, # E,  49/39
        1.3333333333333333, # F,  4/3
        1.4100719424460430, # F#, 196/139
        1.4961832061068703, # G,  196/131
        1.5806451612903225, # G#, 49/31
        1.6752136752136753, # A,  196/117
        1.7818181818181817, # A#, 98/55
        1.8846153846153846  # B,  49/26
    ]
}

def diff_of_notes(base_octace, base_note, audible_octave, audible_note):
    base = base_octace * len(notes) + base_note
    audible = audible_octave * len(notes) + audible_note

    diff = audible - base

    return diff // len(notes), diff % len(notes)


def recalculate_octave_and_note(octave, note):
    if note < 0:
        note += len(notes)
        octave -= 1

        # We were already on the lowest octave
        if octave < 0:
            return 0, 0

        # Note up, octave down
        return note, octave

    if note >= len(notes):
        note -= len(notes)
        octave += 1

        # We were already on the highest octave
        if octave > highest_octave:
            return len(notes) - 1, highest_octave

        # Note down, octave up
        return note, octave

    return note, octave


class RocketTuner:
    def __init__(self):
        self.base_freq = default_freq
        self.temperament = default_temperament

        self.base_note = default_note
        self.base_octave = default_octave

        self.audible_note = default_note
        self.audible_octave = default_octave

        Gst.init_check()

        self.audio = Gst.Pipeline(name='note')
        self.audio_source = Gst.ElementFactory.make('audiotestsrc', 'src')
        sink = Gst.ElementFactory.make('autoaudiosink', 'output')

        self.audio.add(self.audio_source)
        self.audio.add(sink)
        self.audio_source.link(sink)

        self.gladefile = 'rockettuner.glade'
        self.builder = Gtk.Builder()

        self.builder.add_from_file(self.gladefile)
        self.builder.connect_signals(self)

        self.window = self.builder.get_object('rockettuner')
        self.window.set_icon_from_file('icon/icon64.png')

        self.base_freq_obj = self.builder.get_object('entry_base_freq')
        self.base_note_obj = self.builder.get_object('combo_base_note')
        self.base_octave_obj = self.builder.get_object('combo_base_octave')

        self.audible_freq_obj = self.builder.get_object('label_audible_freq')
        self.audible_note_obj = self.builder.get_object('combo_audible_note')
        self.audible_octave_obj = self.builder.get_object('combo_audible_octave')

        self.temperament_obj = self.builder.get_object('combo_temperament')

        self.play_obj = self.builder.get_object('button_play')

        self.update_fields()

    def update_fields(self):
        self.set_base_freq(self.base_freq)

        self.base_note_obj.set_active(self.base_note)
        self.base_octave_obj.set_active(self.base_octave)

        self.audible_note_obj.set_active(self.audible_note)
        self.audible_octave_obj.set_active(self.audible_octave)

    def set_base_freq(self, value):
        if 0.0 < value <= 22100.0:
            self.base_freq = value
            self.base_freq_obj.set_text(str(self.base_freq))

            self.set_audible_freq()

    def set_audible_freq(self):
        octaves, notes = diff_of_notes(
            self.base_octave,
            self.base_note,
            self.audible_octave,
            self.audible_note,
        )

        value = self.base_freq * (2 ** octaves) * temperaments[self.temperament][notes]

        self.audio_source.set_property('freq', value)
        self.audible_freq_obj.set_text(f'Audible frequency: {value:.2f} Hz')

    def set_base_note(self, offset):
        note = self.base_note + offset

        note, octave = recalculate_octave_and_note(self.base_octave, note)

        self.base_note = note
        self.base_octave = octave

        self.update_fields()

    def set_audible_note(self, offset):
        note = self.audible_note + offset

        note, octave = recalculate_octave_and_note(self.audible_octave, note)

        self.audible_note = note
        self.audible_octave = octave

        self.update_fields()

    def on_base_freq_changed(self, obj):
        try:
            self.set_base_freq(float(obj.get_text()))
        except ValueError:
            pass

    def on_base_freq_next(self, obj):
        self.set_base_freq(self.base_freq + 1.0)

    def on_base_freq_prev(self, obj):
        self.set_base_freq(self.base_freq - 1.0)

    def on_base_note_scrolled(self, obj, event):
        if event.direction == Gdk.ScrollDirection.DOWN:
            self.set_base_note(1)
        else:
            self.set_base_note(-1)

        # Inhibit default operation
        return True

    def on_base_note_changed(self, obj):
        self.base_note = self.base_note_obj.get_active()

        self.update_fields()

    def on_base_note_next(self, obj):
        self.set_base_note(1)

    def on_base_note_prev(self, obj):
        self.set_base_note(-1)

    def on_base_octave_scrolled(self, obj, event):
        direction = event.direction

        if direction == Gdk.ScrollDirection.DOWN:
            self.set_base_note(12)
        else:
            self.set_base_note(-12)

        # Inhibit default operation
        return True

    def on_base_octave_changed(self, obj):
        self.base_octave = self.base_octave_obj.get_active()

        self.update_fields()

    def on_base_octave_next(self, obj):
        self.set_base_note(12)

    def on_base_octave_prev(self, obj):
        self.set_base_note(-12)

    def on_audible_note_scrolled(self, obj, event):
        if event.direction == Gdk.ScrollDirection.DOWN:
            self.set_audible_note(1)
        else:
            self.set_audible_note(-1)

        # Inhibit default operation
        return True

    def on_audible_note_changed(self, obj):
        self.audible_note = self.audible_note_obj.get_active()

        self.update_fields()

    def on_audible_note_next(self, obj):
        self.set_audible_note(1)

    def on_audible_note_prev(self, obj):
        self.set_audible_note(-1)

    def on_audible_octave_scrolled(self, obj, event):
        if event.direction == Gdk.ScrollDirection.DOWN:
            self.set_audible_note(12)
        else:
            self.set_audible_note(-12)

        # Inhibit default operation
        return True

    def on_audible_octave_changed(self, obj):
        self.audible_octave = self.audible_octave_obj.get_active()

        self.update_fields()

    def on_audible_octave_next(self, obj):
        self.set_audible_note(12)

    def on_audible_octave_prev(self, obj):
        self.set_audible_note(-12)

    def on_temperament_changed(self, obj):
        self.temperament = self.temperament_obj.get_active_text()

        self.set_audible_freq()

    def on_play(self, obj):
        if obj.get_active():
            self.audio.set_state(Gst.State.PLAYING)
        else:
            self.audio.set_state(Gst.State.NULL)

    def on_key_press(self, obj, event):
        if event.state & Gdk.ModifierType.CONTROL_MASK:
            scancode = event.get_scancode()

            if scancode == 65: # space
                self.play_obj.clicked()
            elif scancode == 111: # up
                self.set_audible_note(12)
            elif scancode == 116: # down
                self.set_audible_note(-12)
            elif scancode == 113: # left
                self.set_audible_note(-1)
            elif scancode == 114: # right
                self.set_audible_note(1)

            return True

        return False

    def on_delete_window(self, obj, event):
        Gtk.main_quit()

    def show_all(self):
        self.window.show()

if __name__ == '__main__':

    window = RocketTuner()
    window.show_all()

    Gtk.main()
