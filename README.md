# RocketTuner
## It rocks!
__Hyper-Spychoacoustic Tuning Application (HSTA).__

Actually it's a rocket-fast sine-wave generator for musical pitch identification/correction purposes. It's fine to produce audible, physically accurate sine tones and scales for musical instrument tuning. With good  loudspeakers, headphones (recommended) with proper frequency range and physical position, the user can compare the real life voice/sound pitch with the digital source from RocketTuner.

## It's good for You.
__Example - Tuning a Pipe Organ__

To avoid the Masking Effects during a musical instrument tuning sometimes we need to move around in the physical space, and finally when we are in an ideal position we can not hear properly the Tuning Stop (without spychoacoustic distortion), so it's better to put it directly on our ears. Naturally secondary checking/verification procedure is recommended via listening from an other ideal position or continous monitoring with the help reliable sensors positioned properly.
